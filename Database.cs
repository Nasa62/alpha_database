//
// Title: Alpha Database (AlphaDB)
// Author: Mr. Doom (5621)
// 

// PLEASE NOTE: Check to make sure a field/row exists BEFORE adding/removing/updating it, as the secondary checks MAY be removed in a later version for speed optimization.


//
// Initializes the database once the object is created
// Requires that the following fields be set on the object
// -: name
// -: path (must be "client" or "server")
function Alpha_DB::onAdd(%this)
{
	%this.loadedOnce = false;

	//Check to see if we have a name.
	if(%this.name $= "")
	{
		if($Dev::Alpha::Database::DebugMode) error("No database name specified!");
		%this.delete();
		return;
	}
	if(%this.path !$= "client" && %this.path !$= "server") // I had SOME reasoning behind this restriction, along with a beer, don't know what it was, mmm beer.
	{
		if($Dev::Alpha::Database::DebugMode) error("Invalid storage path! Must be either client or server");
		%this.delete();
		return;
	}

	%this.rowsList = new ScriptObject(){ class = Alpha_LinkedList; };
	%this.fieldsList = new ScriptObject(){ class = Alpha_LinkedList; };
	//Check to see if DB file exists.
	%this.saveFile = "config/" @ %this.path @ "/" @ %this.name @ ".dat";
	if(isFile(%this.saveFile))
	{
		if(!%this.load())
		{
			if($Dev::Alpha::Database::DebugMode) error("Load database failure!");
			%this.delete();
			return;
		}
	}

	// Begin ticking
	%this.tick(true); // True for initial tick (don't save)
}

//
// Cleans up the database memory usage upon deletion
// TODO: Should make final save of the system!
 function Alpha_DB::onRemove(%this)
{
	// Do something...
}

//
// It like searches the database bro. It very slow.
// Inputs are:
// 	STRING: Query - the query string CASE INSENSITIVE
//	FRIELD: 
function Alpha_DB::search(%this, %query, %field, %partialMatch, %returnList)
{
	if(!%this.isField(%name))
		return -1;
	if(%returnList)
		%result = new ScriptObject() { class = Alpha_LinkedList; };
	%iter = %this.rowsList.getIterator();
	while(%iter.hasNext())
	{
		%row = %iter.next();
		if(%partialMatch)
			if(striPos(%this.row[%row, %field], %query) != -1)
				if(%returnList)
					%result.push(%row);
				else
					return %row;
		else
			if(%this.row[%row, %field] $= %query)
				if(%returnList)
					%result.push(%row);
				else
					return %row;
	}
	if(%returnList && !%result.isEmpty())
		return %result;
	return -1;
}

function Alpha_DB::tick(%this, %initial)
{
	cancel(%this.tick);
	if(!%initial)
		%this.save();
	%this.tick = %this.schedule(600000, "tick", false);
}

// TABLE DATA SYSTEM

// Fields
function Alpha_DB::addField(%this, %name, %default)
{
	if(%this.isField(%name))
		return false;
	%this.fieldsList.push(%name);
	%this.fieldsDefault[%name] = %default;
	return true;
}

function Alpha_DB::updateFieldDefault(%this, %name, %default)
{
	if(!%this.isField(%name))
		return false;
	%this.fieldsDefault[%name] = %default;
	return true;
}

function Alpha_DB::getFieldDefault(%this, %name)
{
	return %this.fieldsDefault[%name];
}

function Alpha_DB::isField(%this, %name)
{
	%iter = %this.fieldsList.getIterator();
	while(%iter.hasNext())
	{
		if(%iter.next() $= %name)
			return true;
	}
	return false;
}

function Alpha_DB::removeField(%this, %name)
{
	if(!%this.isField(%name))
		return false;
	%iter = %this.fieldsList.getIterator();
	while(%iter.hasNext())
	{
		if(%iter.next() $= %name)
		{
			%iter.remove();
			break;
		}
	}
	%iter.delete();
//	%iter = %this.rowsList.getIterator(); // MEMORY CLEANUP (Makes a difference on large servers running for a long time, but takes a bit longer, NOTE: fix memory problems elsewhere)
//	while(%iter.hasNext())
//	{
//		%row = %iter.next();
//		%this.row[%row, %name] = "";
//	}

	return true;
}

//ROWS

function Alpha_DB::addRow(%this, %row)
{
	if(%this.isRow(%row))
		return false;
	%this.rowsList.push(%row);
	%this.row[%row, "exists"] = true;
}

function Alpha_DB::isRow(%this, %row)
{
	return %this.row[%row, "exists"];
}

function Alpha_DB::removeRow(%this, %row)
{
	if(!%this.isRow(%row))
		return false;
	%iter = %this.rowsList.getIterator();
	while(%iter.hasNext())
	{
		if(%iter.next() $= %row)
		{
			%iter.remove();
			break;
		}
	}
	%iter.delete();
	%this.row[%row, "exists"] = "";

	// Memory cleanup:
//	%iter = %this.fieldsList.getIterator();
//	while(%iter.hasNext())
//	{
//		%field = %iter.next();
//		%this.row[%row, %field, "exists"] = "";
//		%this.row[%row, %field] = "";
//	}
//	%iter.delete();

	return true;
}

// Accessors

function Alpha_DB::getStore(%this, %row, %field)
{
	if(!%this.isField(%field) || !%this.isRow(%row))
		return false;
	if(!%this.row[%row, %field, "exists"])
	{
		%this.row[%row, %field, "exists"] = true;
		%this.row[%row, %field] = %this.fieldsDefault[%field];
	}
	return %this.row[%row, %field];
}

function Alpha_DB::setStore(%this, %row, %field, %data)
{
	if(!%this.isField(%field) || !%this.isRow(%row))
		return false;
	%this.row[%row, %field, "exists"] = true;
	%this.row[%row, %field] = %data;
	return true;
}

// Saving
function Alpha_DB::save(%this)
{
	// Open file
	%file = new FileObject();
	%file.openForWrite(%this.saveFile);

	// Write header
	%file.writeLine("ALPHADB_CSV");

	// Prepare field information
	%header_FieldNames = "\"FIELDNAMES\"";
	%header_FieldDefaults = "\"FIELDDEFAULTS\"";

	%iter = %this.fieldsList.getIterator();
	while(%iter.hasNext())
	{
		%field = %iter.next();
		%header_FieldNames = %header_FieldNames @ ",\"" @ %field @ "\"";
		%header_FieldDefaults = %header_FieldDefaults @ ",\"" @ %this.fieldsDefault[%field] @ "\"";
	}
	%iter.delete();

	// Write field information
	%file.writeLine(%header_FieldNames);
	%file.writeLine(%header_FieldDefaults);

	// Write Row Data
	%iter = %this.rowsList.getIterator();
	while(%iter.hasNext())
	{
		%row = %iter.next();
		if($Dev::Dependencies::Database::DebugMode) warn(" Writing row: \"" @ %row @ "\"");
		%line = "\"" @ %row @ "\"";
		%iter_fields = %this.fieldsList.getIterator();
		while(%iter_fields.hasNext())
		{
			%field = %iter_fields.next();
			%line = %line @ ",\"" @ expandEscape(%this.getStore(%row, %field)) @ "\"";
		}
		%iter_fields.delete();
		%file.writeLine(%line);
	}
	%iter.delete();

	%file.close();
	%file.delete();
}

// Loading
function Alpha_DB::hasLoadedOnce(%this)
{
	return %this.loadedOnce;
}

function Alpha_DB::load(%this)
{
	// Open file
	%file = new FileObject();
	%file.openForRead(%this.saveFile);

	// Read first line:
	%line = %file.readLine();
	switch$(getWord(%line, 0))
	{
		case "ALPHADB_CSV":
			%fieldNamesIdentifier = "FIELDNAMES";
			%fieldDefaultsIdentifier = "FIELDDEFAULTS";
			%this.load_CSV(%fieldNamesIdentifier, %fieldDefaultsIdentifier);
		case "SIMPLEDB_CSV":
			warn("SimpleDB CSV files not 100% supported, attempting to load!");
			%fieldNamesIdentifier = "KEY";
			%fieldDefaultsIdentifier = "DEF";
			%this.load_CSV(%fieldNamesIdentifier, %fieldDefaultsIdentifier);
		case "[DDBCL]":
			error("DDBCL command files are not supported by this parser! Aborting.");
			return false;
		case "!":
			error("SimpleDB v1-3 save files are not supported by this parser (yet)! Aborting.");
			return false;
		case "values":
			error("SASSY? HOW DARE YOU.");
			return false;
		default:
			error("Alpha_DB cannot identify the database save file format! Aborting.");
			return false;
	}
	%file.close();
	%file.delete();

	%this.loadedOnce = true;
}

function Alpha_DB::load_CSV(%this, %fieldNamesIdentifier, %fieldDefaultsIdentifier)
{
	// Open file
	%file = new FileObject();
	%file.openForRead(%this.saveFile);

	// Skip first line:
	%file.readLine();

	// Find field names
	%fieldNames = "";
	while(!%file.isEOF())
	{
		%list = %this.CSV2LinkedList(%file.readLine());
		if(%list.shift() $= %fieldNamesIdentifier)
		{
			%fieldNames = %list;
			break;
		}
	}

	// Find field defaults
	%fieldDefaults = "";
	while(!%file.isEOF())
	{
		%list = %this.CSV2LinkedList(%file.readLine());
		if(%list.shift() $= %fieldDefaultsIdentifier)
		{
			%fieldDefaults = %list;
			break;
		}
	}

	if(%fieldNames.size() != %fieldDefaults.size())
	{
		error("FieldNames Count and FieldDefaults Count Mismatch! Aborting.");
		return false;
	}

	// Add fields and defaults to database
	%iter_names = %fieldNames.getIterator();
	%iter_defaults = %fieldDefaults.getIterator();
	while(%iter_names.hasNext())
	{
		%name = %iter_names.next();
		%default = %iter_defaults.next();
		%this.addField(%name, %default);
	}
	%iter_names.delete();
	%iter_defaults.delete();

	// Load rows and add to database
	while(!%file.isEOF())
	{
		%list = %this.CSV2LinkedList(%file.readLine());
		%row = %list.shift();
		
		if(%list.size() != %fieldNames.size())
		{
			error("ROW: " @ %row @ " field size mismatch! Skipping row.");
			continue;
		}

		%this.addRow(%row); // Add row to database.

		%iter_fields = %fieldNames.getIterator();
		%iter_rowdata = %list.getIterator();
		while(%iter_fields.hasNext())
		{
			%field = %iter_fields.next();
			%data = %iter_rowdata.next();
			%this.setStore(%row, %field, %data); // Add data to database
		}
	}

	%file.close();
	%file.delete();

	return true;
}

function Alpha_DB::CSV2LinkedList(%this, %string) // Converts a single line of CSV data to a linked list (Hopefully)
{
	%list = new ScriptObject(){ class = Alpha_LinkedList; };
	%string = strReplace(%string, "\\\"", "&DBQUOTE");

	while(true)
	{
		%element = "";
		%commaPos = strPos(%string, ",");
		%quotePos = strPos(%string, "\"");
		if(%commaPos < 0)
		{
			if(strLen(%string) != 0)
				%element = %string;
			else
				return %list; // End of Line
		}
		if((%quotePos < %commaPos || %commaPos == -1) && %quotePos != -1)
		{
			%string = getSubStr(%string, %quotePos + 1, strLen(%string));
			%endingQuotePos = strPos(%string, "\"");
			if(%endingQuotePos < 0)
				return -1;
			%element = getSubStr(%string, 0, %endingQuotePos);
			%nextCommaPos = strPos(%string, ",", %endingQuotePos);
			if(%nextCommaPos != -1)
				%string = getSubStr(%string, %nextCommaPos + 1, strLen(%string));
			else
				%string = "";
		}
		else
		{
			%string = getSubStr(%string, %commaPos + 1, strLen(%string));
		}
		%element = strReplace(%element, "&DBQUOTE", "\\\"");
		%element = collapseEscape(%element);
		%list.push(%element);
	}
}
