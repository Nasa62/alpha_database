// Class Alpha_LinkedList

function Alpha_LinkedList::onAdd(%this)
{
	%this.count = 0;
	%this.first = null;
	%this.last = null;
}

function Alpha_LinkedList::linkNodes(%this, %a, %b, %c)
{
	if(isObject(%b)) // We are linking a new node
	{
		if(!isObject(%a)) // Forward Link
			%this.first = %b; // a does not exist, b must be the first node in the entire list. Point first at b
		else
			%a.next = %b; // link a->b
		if(!isObject(%c)) // Reverse Link
			%this.last = %b; // c does not exist, b must be the last node in the entire list. Point last at b
		else
			%c.prev = %b; // link b<-c
		%b.next = %c; // link b->c
		%b.prev = %a; // link a<-b
	}
	else // We are removing a node, and relinking the surrounding nodes.
	{
		if(!isObject(%a)) // Forward Link
			%this.first = %c; // a does not exist, c must be the only node in the entire list. Point first at c
		else
			%a.next = %c; // link a->c
		if(!isObject(%c)) // Reverse Link
			%this.last = %a; // c does not exist, a must be the last node in the entire list. Point last at a
		else
			%c.prev = %a; // link a<-c
	}
}

function Alpha_LinkedList::unshift(%this, %element)
{
	%this.addBefore(%this.first, %element);
}

function Alpha_LinkedList::push(%this, %element)
{
	%this.addAfter(%this.last, %element);
}

function Alpha_LinkedList::createNode(%this, %element)
{
	%node = new ScriptObject(){};
	%node.contents = %element;
	return %node;
}

function Alpha_LinkedList::addAfter(%this, %node, %element)
{
	%newNode = %this.createNode(%element);
	%this.count++;
	if(!isObject(%node))
		%this.linkNodes(null, %newNode, null); // the list is empty, point %this.first->newNode
	else
		%this.linkNodes(%node, %newNode, %node.next); // Link node<->newNode<->node.next
}

function Alpha_LinkedList::addBefore(%this, %node, %element)
{
	%newNode = %this.createNode(%element);
	%this.count++;
	if(!isObject(%node))
		%this.linkNodes(null, %newNode, null); // the list is empty, point %this.first->newNode
	else
		%this.linkNodes(%node.prev, %newNode, %node); // Link node.prev<->newNode<->node
}

function Alpha_LinkedList::removeNode(%this, %node)
{
	if(!isObject(%node))
		return;
	%this.linkNodes(%node.prev, null, %node.next);
	%contents = %node.contents;
	%node.delete();
	%this.count--;
	return %contents;
}

function Alpha_LinkedList::shift(%this)
{
	return %this.removeNode(%this.first);
}

function Alpha_LinkedList::pop(%this)
{
	return %this.removeNode(%this.last);
}

function Alpha_LinkedList::isEmpty(%this)
{
	return %this.count == 0;
}

function Alpha_LinkedList::size(%this)
{
	return %this.count;
}

function Alpha_LinkedList::getIterator(%this)
{
	return new ScriptObject(){class=Alpha_LinkedList_Iterator; list=%this;};
}

// Class Alpha_LinkedList_Iterator

function Alpha_LinkedList_Iterator::hasNext(%this)
{
	return !isObject(%this.current) ? isObject(%this.list.first) : isObject(%this.current.next);
}

function Alpha_LinkedList_Iterator::hasPrev(%this)
{
	return !isObject(%this.current) ? false : isObject(%this.current.prev);
}

function Alpha_LinkedList_Iterator::next(%this)
{
	if(!%this.hasNext())
		return; // NoSuchElement
	%this.current = !isObject(%this.current) ? %this.list.first : %this.current.next;
	return %this.current.contents;
}

function Alpha_LinkedList_Iterator::prev(%this)
{
	if(!%this.hasPrev())
		return; // NoSuchElement
	%this.current = %this.current.prev;
	return %this.current.contents;
}

function Alpha_LinkedList_Iterator::add(%this, %element)
{
	if(!isObject(%this.current))
		%this.list.unshift(%element);
	else
		%this.list.addAfter(%this.current, %element);
}

function Alpha_LinkedList_Iterator::remove(%this)
{
	if(!isObject(%this.current))
		return; // IllegalState
	%prevNode = %this.current.prev;
	%this.list.removeNode(%this.current);
	%this.current = %prevNode;
}

function Alpha_LinkedList_Iterator::set(%this, %element)
{
	if(!isObject(%this.current))
		return; // IllegalState
	%this.current.contents = %element;
}
